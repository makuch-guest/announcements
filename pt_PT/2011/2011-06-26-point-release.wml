# Status: [sent]
<define-tag pagetitle>Actualização da versão Debian 6.0: 6.0.2</define-tag>
<define-tag release_date>2011-06-26</define-tag>
#use wml::debian::news
#use wml::debian::translation-check translation="1.1"
# $Id:

<define-tag release>6.0</define-tag>
<define-tag codename>squeeze</define-tag>
<define-tag revision>6.0.2</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="http://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td><td align="left">%3</td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="http://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="http://packages.debian.org/src:%0">%0</a></define-tag>

<p>O projecto Debian congratula-se em anunciar a segunda actualização da sua 
distribuição estável Debian 6.0 (nome de código <q><codename></q>).
Esta actualização adiciona primariamente correcções para problemas de 
segurança à versão estável, assim como alguns ajustes a problemas sérios. 
Avisos de segurança foram publicados separadamente e estão referenciados quando 
disponíveis.</p>

<p>Por favor note que esta actualização não constitui uma nova versão da Debian 
6.0 mas apenas actualizações a alguns pacotes incluídos. Não existe necessidade 
de atirar fora os 6.0 CDs ou DVDs, mas apenas actualizar através de um 'mirror' 
Debian actualizado, depois da instalação, para que qualquer pacote desactualizado 
seja efectivamente actualizado.</p>

<p>Àqueles que frequentemente fazem actualizações a partir de security.debian.org, 
não precisam de actualizar muitos pacotes e a maior parte das actualizações com 
origem em security.debian.org estão já incluídas nesta actualização.</p>

<p>Novo suporte de instalação bem como imagens de CD e DVD contendo pacotes 
actualizados estarão disponíveis brevemente nos locais habituais.</p>

<p>A actualização online para esta versão é normalmente efectuada apontando a 
ferramenta dos pacotes aptitude (ou apt) (veja a página do manual sources.list(5))
para um dos muitos 'mirrors' FTP ou HTTP da Debian.  Uma lista extensiva de 
'mirrors' está disponível em :</p>

<div class="center">
  <a href="$(HOME)/mirror/list">http://www.debian.org/mirror/list</a>
</div>


<h2>Miscelânea de correcções de falhas (bugs)</h2>

<p>Esta actualização estável adiciona algumas correcções importantes aos 
seguintes pacotes:</p>
<table border=0>
<tr><th>Package</th>               <th>Reason</th></tr>
<correction aide                             "Suporta adequadamente ficheiros de grande dimensão em sistemas 32-bit; corrige o grupo para os ficheiros de 'log' para o bind9">
<correction approx                           "Não tenta fazer a cache de ficheiros InRelease ou não comprimidos non-.gz">
<correction apr                              "Corrige a dimensão dinâmica de apr_ino_t dependendo de -D_FILE_OFFSET_BITS no kfreebsd-*">
<correction apt                              "Corrige o cálculo da dimensão de ficheiro em arquitecturas big-endian; não alerta para a re-inserção do CD no "apt-get update"; adiciona suporte XZ">
<correction apt-listchanges                  "Manuseia ficheiros NEWS correctamente contendo apenas uma entrada">
<correction base-files                       "Actualiza /etc/debian_version">
<correction clive                            "Adaptação para alterações liveleak.com">
<correction dbus                             "Corrige o DoS local para serviços de sistema (CVE-2011-2200)">
<correction deborphan                        "Excluí o libreoffice da saída (output) --guess-section; detecta o WINCH num modo POSIX; tradução de pequenas correcções nas traduções">
<correction dokuwiki                         "Corrige uma questão no contorno (bypass) da interface XMLRPC">
<correction dpkg                             "Corrige uma regressão em 'dpkg-divert --rename'; dpkg-split: não corrompe metadados em sistemas 32-bit; corrige declaração compat vsnprintf()">
<correction e2fsprogs                        "Várias correcções de falhas">
<correction fakechroot                       "Corrige 'debootstrap --variant=fakechroot'">
<correction fcgiwrap                         "Corrige o alvo de init script's 'stop'">
<correction gdm3                             "Faz o reset do 'handler' SIGPIPE antes do inicio da sessão; executa o 'script' PostSession mesmo quando o GDM é terminado ou desligado">
<correction git                              "Permite remover e purgar num passo, terminando o serviço git-daemon/log antes de remover o utilizador do  gitlog">
<correction gnome-settings-daemon            "Contorno da possível condição de concorrência ao iniciar o gestor Xsettings">
<correction ia32-libs                        "Refrescar pacotes da versão estável e de proposed-updates.">
<correction iceowl                           "Actualizações de segurança">
<correction im-config                        "Evita falha do login através do GDM se o im-config for removido mas não purgado">
<correction inn                              "Deixa de usar 'sort +1n' em makehistory; desactiva a opção por omissão desactualizada CHECK_INCLUDED_TEXT">
<correction josm                             "Oferece explicação mais detalhada ao utilizadores que não concordam com a nova licença OSM">
<correction kde4libs                         "Corrige a segurança de certificados Wildcard SSL e XSS; corrige a checksum do ktar e corrige o link longo UTF-8">
<correction kdenetwork                       "Melhora a correcção para a questão transversal do directório CVE-2010-1000">
<correction kernel-wedge                     "Adiciona hpsa e pm8001 aos scsi-extra-modules; adiciona bna aos nic-extra-modules">
<correction kerneltop                        "Aumenta a dimensão do buffer de linhas para 1024 bytes">
<correction klibc                            "ipconfig: permite escapar às opções DHCP e gere correctamente múltiplos dispositivos de rede ligados CVE-2011-1930)">
<correction krb5                             "Corrige DoS; corrige inter-operabilidade com w2k8r2 KDCs; corrige libertação não válida e dupla libertação de memória; não ocorre falha de autenticação se a verificação do PAC falhar">
<correction kupfer                           "Utiliza o tipo de parâmetro correcto para permitir que as 'keybindings' corram de novo">
<correction libapache2-mod-perl2             "Reconstrução de acordo com apr o 1.4.2-6+squeeze3 para corrigir a dimensão do apr_ino_t no kFreeBSD">
<correction libburn                          "Não cria imagens com permissões overly-restrictive">
<correction libfinance-quotehist-perl        "Desactiva a suite de teste, danificada por alterações do site web">
<correction libmms                           "Corrige questões de alinhamento no arm">
<correction linux-2.6                        "Novo suporte a hardware; adiciona o longterm 2.6.32.41; corrige do oops através de tabelas de partição corruptas">
<correction linux-kernel-di-amd64-2.6        "Reconstrução de acordo com o kernel-wedge 2.74+squeeze3">
<correction linux-kernel-di-armel-2.6        "Reconstrução de acordo com o kernel-wedge 2.74+squeeze3">
<correction linux-kernel-di-i386-2.6         "Reconstrução de acordo com o kernel-wedge 2.74+squeeze3">
<correction linux-kernel-di-ia64-2.6         "Reconstrução de acordo com o kernel-wedge 2.74+squeeze3">
<correction linux-kernel-di-mips-2.6         "Reconstrução de acordo com o kernel-wedge 2.74+squeeze3">
<correction linux-kernel-di-mipsel-2.6       "Reconstrução de acordo com o kernel-wedge 2.74+squeeze3">
<correction linux-kernel-di-powerpc-2.6      "Reconstrução de acordo com o kernel-wedge 2.74+squeeze3">
<correction linux-kernel-di-s390-2.6         "Reconstrução de acordo com o kernel-wedge 2.74+squeeze3">
<correction linux-kernel-di-sparc-2.6        "Reconstrução de acordo com o kernel-wedge 2.74+squeeze3">
<correction lua-expat                        "Corrige o ataque DoS 'billion laughs'">
<correction monkeysphere                     "Corrige o monkeysphere-host revoke-key">
<correction nagios-plugins                   "Aloca um buffer suficiente para gerir todos os IPs dos 'hosts' a serem 'pinged'">
<correction nsd3                             "Remove o statoverride antes de remover o pacote do utilizador">
<correction openldap                         "Corrige possíveis questões de corrupção de base de dados, algumas questões de segurança e do dpkg-reconfigure">
<correction php-svn                          "Reconstrução de acordo com o apr 1.4.2-6+squeeze3 para corrigir a dimensão do apr_ino_t no kFreeBSD">
<correction php5                             "Reconstrução de acordo com o apr 1.4.2-6+squeeze3 para corrigir a dimensão do apr_ino_t no kFreeBSD">
<correction pianobar                         "Actualiza as chaves API para o XMLRPC v30">
<correction postgresql-8.4                   "Novo lançamento 'upstream' de correcção de falhas; corrige o pg_upgrade de modo a usar tabelas TOAST">
<correction prosody                          "Corrige o ataque DoS 'billion laughs'">
<correction puppet                           "Corrige o fornecedor de serviços para apropriadamente utilizar o update-rc.d para desactivar a API">
<correction python-apt                       "Impede multiarch por omissão em RealParseDepends; adiciona suporte XZ">
<correction python-gudev                     "Adiciona uma dependência perdida em python-gobject">
<correction q4wine                           "Pára de enviar a 'library' em lib64">
<correction qemu                             "Não regista o qemu-mips(el) com o binfmt em mips(el)">
<correction qemu-kvm                         "Corrige a divisão por 0 em alguns clientes; corrige o vnc zlib 'overflow'; não aborta nos erros de hardware do utilizador; corrige a migração para 32-bit">
<correction qt4-x11                          "Colocação na lista negra de alguns certificados SSL fraudulentos; corrige fragilidades na verificação de certificados de 'wilcards'">
<correction rapidsvn                         "Reconstrução de acordo com o apr 1.4.2-6+squeeze3 para corrigir a dimensão do apr_ino_t no kFreeBSD">
<correction refpolicy                        "Várias correcções das permissões">
<correction reprepro                         "Manuseia ficheiros de lançamento (Release) que não contenham md5sums">
<correction ruby1.8                          "Corrige actualizações a partir do lenny fazendo o libruby1.8 entrar em conflicto/substituir o irb1.8 e o rdoc1.8">
<correction samba                            "Corrige erro de símbolo não definido de tdb2.so; algumas falhas relacionadas com a impressão e um vazamento (leak) da gid em winbind / idmap. Documentada uma nova e potencial perturbante 'map untrusted to domain'">
<correction schroot                          "Corrige o carregamento de dchroot.conf">
<correction softhsm                          "Remove entradas statoverride antes do pacote do utilizador">
<correction sun-java6                        "Nova actualização de segurança 'upstream'">
<correction tzdata                           "Nova actualização de segurança 'upstream'">
<correction vimperator                       "Resolve questões de compatibilidade com o iceweasel">
<correction widelands                        "Corrige problemas potenciais de segurança em jogos de Internet">
<correction xenomai                          "Adapta o patch kernel de modo a ser aplicado de modo limpo ao kernel squeeze">
<correction xserver-xorg-video-tseng         "Corrige iniciação do controlador">
</table>

<h2>Instalador Debian</h2>

<p>A nova imagem kernel utilizada pelo instalador foi actualizada de modo 
a incorporar um número importante de correcções de segurança, juntamente 
com suporte a hardware adicional.</p>

<h2>Actualizações de Segurança</h2>


<p>Esta revisão acrescenta as actualizações de segurança seguintes à versão 
estável. A Equipa de Segurança lançou já um aviso para cada uma destas 
actualizações:</p>

<table border=0>
<tr><th>ID Aviso</th>  <th>Pacote</th>    <th>Correcção</th></tr>


<dsa 2011 2161 openjdk-6               "Negação de serviço">
<dsa 2011 2193 libcgroup               "Vários">
<dsa 2011 2194 libvirt                 "Escalada de privilégios">
<dsa 2011 2195 php5                    "Vários">
<dsa 2011 2197 quagga                  "Negação de serviço">
<dsa 2011 2198 tex-common              "Limpeza de entrada de dados insuficiente">
<dsa 2011 2199 iceape                  "Actualização da lista negra de certificados HTTPS">
<dsa 2011 2200 iceweasel               "Actualização da lista negra de certificados HTTPS">
<dsa 2011 2201 wireshark               "Vários">
<dsa 2011 2202 apache2                 "Falha em perder os privilégios de root">
<dsa 2011 2203 nss                     "Actualização da lista negra de certificados HTTPS">
<dsa 2011 2205 gdm3                    "Escalada de privilégios">
<dsa 2011 2206 mahara                  "Vários">
<dsa 2011 2208 bind9                   "Negação de serviço">
<dsa 2011 2209 tgt                     "Dupla libertação da zona de memória">
<dsa 2011 2211 vlc                     "Limpeza de entrada de dados em falta">
<dsa 2011 2212 tmux                    "Escalada de privilégios">
<dsa 2011 2213 x11-xserver-utils       "Limpeza de entrada de dados em falta">
<dsa 2011 2214 ikiwiki                 "Validação de dados de entrada em falta">
<dsa 2011 2215 gitolite                "Transversal a directórios">
<dsa 2011 2216 isc-dhcp                "Limpeza de entrada de dados em falta">
<dsa 2011 2218 vlc                     "Heap-based buffer overflow">
<dsa 2011 2219 xmlsec1                 "Sobre-escrita de ficheiro">
<dsa 2011 2220 request-tracker3.8      "Vários">
<dsa 2011 2221 libmojolicious-perl     "Transversal a directórios">
<dsa 2011 2222 tinyproxy               "Processamento incorrecto das ACL">
<dsa 2011 2223 doctrine                "Injecção SQL">
<dsa 2011 2224 openjdk-6               "Vários">
<dsa 2011 2225 asterisk                "Vários">
<dsa 2011 2226 libmodplug              "Buffer overflow">
<dsa 2011 2227 iceape                  "Vários">
<dsa 2011 2229 spip                    "Negação de serviço">
<dsa 2011 2230 qemu-kvm                "Vários">
<dsa 2011 2231 otrs2                   "Cross-site scripting">
<dsa 2011 2232 exim4                   "Vulnerabilidade de formatação de string">
<dsa 2011 2233 postfix                 "Vários">
<dsa 2011 2235 icedove                 "Vários">
<dsa 2011 2236 exim4                   "Injecção de comando">
<dsa 2011 2237 apr                     "Negação de serviço">
<dsa 2011 2238 vino                    "Negação de serviço">
<dsa 2011 2239 libmojolicious-perl     "Vários">
<dsa 2011 2240 user-mode-linux         "Vários vulnerabilidades">
<dsa 2011 2240 linux-2.6               "Vários vulnerabilidades">
<dsa 2011 2241 qemu-kvm                "Erro de implementação">
<dsa 2011 2242 cyrus-imapd-2.2         "Erro de implementação">
<dsa 2011 2244 bind9                   "Condição errada de limites">
<dsa 2011 2245 chromium-browser        "Várias vulnerabilidades">
<dsa 2011 2246 mahara                  "Várias vulnerabilidades">
<dsa 2011 2247 rails                   "Várias vulnerabilidades">
<dsa 2011 2249 jabberd14               "Negação de serviço">
<dsa 2011 2250 citadel                 "Negação de serviço">
<dsa 2011 2254 oprofile                "Injecção de comando">
<dsa 2011 2255 libxml2                 "Buffer overflow">
<dsa 2011 2257 vlc                     "Buffer overflow">
<dsa 2011 2259 fex                     "Contorno de autenticação">
<dsa 2011 2261 redmine                 "Vários">
<dsa 2011 2262 moodle                  "Vários">
<dsa 2011 2263 movabletype-opensource  "Vários">
<dsa 2011 2265 perl                    "Falta de verificação em caso de violação">

</table>

<h2>Pacotes removidos</h2>

<p>Os seguintes pacotes foram removidos devido a circunstâncias para além do nosso
controlo:</p>


<table border=0>
<tr><th>Pacote</th>               <th>Razão</th></tr>

<correction ktsuss                    "problemas de segurança; não mantido">
</table><h2>URL</h2>

<p>Lista completa de pacotes que foram alterados com esta revisão:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>A distribuição estável actual:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/stable/">
</div>

<p>Actualizações propostas à distribuição estável:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/proposed-updates">
</div>

<p>Informação sobre a distribuição estável (notas de lançamento, errata, etc.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">http://www.debian.org/releases/stable/</a>
</div>

<p>Avisos e informação de segurança:</p>

<div class="center">
  <a href="$(HOME)/security/">http://security.debian.org/</a>
</div>


<h2>Sobre Debian</h2>

<p>O Projecto Debian é uma associação de 'developers' de Software Livre que se 
voluntariam o seu tempo e esforço para produzir um sistema operativo 
completamente livre - Debian.</p>


<h2>Informaçao de contacto</h2>

<p>Para mais informação, visite por favor as páginas web da Debian em 
<a href="$(HOME)/">http://www.debian.org/</a>, envie mail para 
&lt;press@debian.org&gt;, ou contacte a equipa da versão estável em 
&lt;debian-release@lists.debian.org&gt;.</p>
